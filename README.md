**Testing**

`clone` the repo and `cd` into the newly created directory

    $ git clone https://gitlab.com/bitco/golang-encrypted-message/
    $ cd golang-encrypted-message
    
Then (assuming go tools are set up on your system) run the following in one 
terminal:

    $ go run tcp-serv.go
    
Then open another terminal and run:

    $ go run tcp-client.go
    
Follow the instructions. The default key is `12341234123412341234123412341234`.

You'll also need to know your computers local IP (the one assigned by your router).

![gif of the app](https://i.imgur.com/q8SwgyS.gif)