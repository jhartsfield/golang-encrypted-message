package main

import (
	"bufio"
	"crypto/aes"
	"crypto/rand"
	"fmt"
	"log"
	"net"
	"os"
)

func main() {
	key := genKey()
	conn := connect()
	msg := mkmsg()
	enc := encrypt([]byte(msg), key)
	conn.Write(enc)
}

// mkmsg() gets the message from stdin
// and returns it to main
func mkmsg() string {
	in := bufio.NewReader(os.Stdin)
	fmt.Print("ENTER MESSAGE: ")
	msg, err := in.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}
	return msg

}

func connect() net.Conn {
	var ip string
	fmt.Print("ENTER ADDR: ")
	fmt.Scanln(&ip)
	conn, err := net.Dial("tcp", ip+":8080")
	if err != nil {
		log.Fatal(err)
	}
	return conn
}

// genKey() asks the user to provide an encryption key,
// if none is provided then a key is generated for them
// and displayed. This key must be sent to the server
// operator for them to decrypt messages
func genKey() []byte {
	var key []byte
	fmt.Print("Please provide a 32 byte AES encryption key or press enter to have one generated: ")
	fmt.Scanln(&key)
	if len([]byte(key)) != 32 {
		fmt.Println("Either no key was entered or the entered key was not 32 bytes in length\nYOUR GENERATED KEY FOR THIS SESSION IS: ")
		key := make([]byte, 32)
		_, err := rand.Read(key)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(key)
	}
	return key
}

// encrypt() encrypts the message, but sometimes the
// message isn't the proper length, so we add padding
func encrypt(msg []byte, key []byte) []byte {
	cipher, err := aes.NewCipher([]byte(key))
	if err != nil {
		log.Fatal(err)
	}

	if len(msg) < cipher.BlockSize() {
		var endLength = cipher.BlockSize() - len(msg)
		ending := make([]byte, endLength, endLength)
		msg = append(msg[:], ending[:]...)
		cipher.Encrypt(msg, msg)
	} else {
		var endLength = len(msg) % cipher.BlockSize()
		ending := make([]byte, endLength, endLength)
		msg = append(msg[:], ending[:]...)
		cipher.Encrypt(msg, msg)
	}
	return msg
}
