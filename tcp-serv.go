package main

import (
	"bufio"
	"crypto/aes"
	"fmt"
	"log"
	"net"
)

func main() {
	ip := getIP()
	ln, err := net.Listen("tcp", ip[:len(ip)-3]+":8080")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Listening @", ip[:len(ip)-3]+":8080...")
	fmt.Println("Waiting for secret messages...\n ")
	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
		}
		go handleConnection(conn)
	}
}

// getIP() gets the systems local IP address
func getIP() string {
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Fatal(err)
	}
	var ip string
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		for _, addr := range addrs {
			if err != nil {
				log.Fatal(err)
			}
			switch v := addr.(type) {
			case *net.IPNet:
				if v.String()[0:3] == "192" || v.String()[0:3] == "10." {
					ip = v.String()
				}
			}
		}
	}
	return ip
}

// When a client connects we read, decrypt, and display the
// message
func handleConnection(conn net.Conn) {
	msg, _ := bufio.NewReader(conn).ReadBytes(16)
	decmsg := decrypt([]byte(msg))
	fmt.Println("✉ Message Received:\n ◯ ",
		string(decmsg),
		"[END]▶▶▶ Waiting for more messages... (ctrl+c to quit)\n")
}

func decrypt(msg []byte) string {
	// Change the key to something secure when using for seccom
	var key string = "12341234123412341234123412341234"
	cipher, err := aes.NewCipher([]byte(key))
	if err != nil {
		log.Fatal(err)
	}
	cipher.Decrypt(msg, msg)
	return string(msg)
}
